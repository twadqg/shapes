package hu.wrd.shapes;

public class Rectangle implements Shape {
    private int x;
    private int y;

    Rectangle(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public double getArea() {
        return x * y;
    }
}
