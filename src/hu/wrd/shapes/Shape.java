package hu.wrd.shapes;

public interface Shape {
    double getArea();
}
