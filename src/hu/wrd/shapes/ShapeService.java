package hu.wrd.shapes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ShapeService {
    private List<Shape> shapeList;

    private void checkIfSameArea()
    {
        for (int i = 0; i < shapeList.size() - 1; i++) {
            for (int j = i + 1; j < shapeList.size(); j++) {
                if (shapeList.get(i).getArea() == shapeList.get(j).getArea() &&
                        (shapeList.get(i).getClass() == shapeList.get(j).getClass() ||
                                (shapeList.get(i).getClass() == Square.class || shapeList.get(i).getClass() == Rectangle.class) && (shapeList.get(j).getClass() == Square.class || shapeList.get(j).getClass() == Rectangle.class)))
                {
                    System.out.println("Error! Input shapes cannot contain the same shapes!");
                }
            }
        }
    }

    private void sortShapeArray(Shape[] array, int firstIndex, int lastIndex) {
        if (firstIndex < lastIndex) {
            double split = array[lastIndex].getArea();

            int i = (firstIndex - 1);
            for (int j = firstIndex; j < lastIndex; j++) {
                if (array[j].getArea() <= split) {
                    i++;
                    Shape temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
            Shape temp = array[i + 1];
            array[i + 1] = array[lastIndex];
            array[lastIndex] = temp;

            int splitIndex = i + 1;

            sortShapeArray(array, firstIndex, splitIndex - 1);
            sortShapeArray(array, splitIndex + 1, lastIndex);
        }
    }

    private void reverseArray(Shape[] array) {
        Shape[] temp = array;
        for (int i = 0; i < array.length; i++)
        {
            array[i] = temp[array.length - 1 - i];
        }
    }

    public ShapeService() {
        this.shapeList = new ArrayList<>();
    }

    public void addShapes(Shape ... shapes) {
        shapeList.addAll(Arrays.asList(shapes));
        checkIfSameArea();
    }

    public void printShapesOrderByAreaAsc() {
        Shape[] temp = new Shape[shapeList.size()];
        int i = 0;
        for (Shape shape : shapeList) {
            temp[i] = shape;
            i++;
        }

        sortShapeArray(temp, 0, temp.length - 1);

        for (Shape shape : temp) {
            System.out.println(shape.getClass());
        }
        System.out.println("printShapesOrderByAreaAsc() called");
    }

    public void printShapesOrderByAreaDesc() {
        Shape[] temp = new Shape[shapeList.size()];
        int i = 0;
        for (Shape shape : shapeList) {
            temp[i] = shape;
            i++;
        }

        sortShapeArray(temp, 0, temp.length - 1);
        reverseArray(temp);

        for (Shape shape : temp) {
            System.out.println(shape.getClass());
        }
        System.out.println("printShapesOrderByAreaDesc() called");
    }
}
