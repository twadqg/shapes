package hu.wrd.shapes;

public class Square implements Shape {
    private int x;

    Square(int x) {
        this.x = x;
    }

    @Override
    public double getArea() {
        return x * 2;
    }
}
